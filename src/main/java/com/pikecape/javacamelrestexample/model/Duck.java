package com.pikecape.javacamelrestexample.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Duck {

    private UUID id;
    private String name;

    public Duck(@JsonProperty("id") UUID id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
