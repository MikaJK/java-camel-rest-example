package com.pikecape.javacamelrestexample.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Flock.
 * 
 * @author Mika J. Korpela
 */
public class Flock {
    private final List<Duck> ducks = new ArrayList<>();

    public List<Duck> getDucks() {
        return ducks;
    }
}
