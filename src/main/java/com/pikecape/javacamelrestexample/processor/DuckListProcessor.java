package com.pikecape.javacamelrestexample.processor;

import com.pikecape.javacamelrestexample.model.Duck;
import com.pikecape.javacamelrestexample.model.Flock;
import java.util.Arrays;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * DuckListProcessor.
 * 
 * @author Mika J. Korpela
 */
public class DuckListProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Flock flock = new Flock();
        Duck[] ducks = exchange.getIn().getBody(Duck[].class);
        flock.getDucks().addAll(Arrays.asList(ducks));
        flock.getDucks().forEach(duck -> {
            System.out.println("DUCK: " + duck.getName());
        });
        exchange.getIn().setBody(flock);
    }
}
