package com.pikecape.javacamelrestexample.processor;

import com.pikecape.javacamelrestexample.model.Duck;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * DuckProcessor.
 * 
 * @author Mika J. Korpela
 */
public class DuckProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Duck duck = exchange.getIn().getBody(Duck.class);
        exchange.getIn().setBody(duck);
    }
}