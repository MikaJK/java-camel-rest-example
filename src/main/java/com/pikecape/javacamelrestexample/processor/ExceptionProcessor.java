package com.pikecape.javacamelrestexample.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * ExceptionProcessor.
 * 
 * @author Mika J. Korpela
 */
public class ExceptionProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
        System.out.println(exception);
    }
    
}
