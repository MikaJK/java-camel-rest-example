package com.pikecape.javacamelrestexample.route;

import com.pikecape.javacamelrestexample.model.Duck;
import com.pikecape.javacamelrestexample.processor.DuckListProcessor;
import com.pikecape.javacamelrestexample.processor.ExceptionProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;

/**
 * SimpleRouteBuilder.
 *
 * @author Mika J. Korpela
 */
public class SimpleRouteBuilder extends RouteBuilder {

    JacksonDataFormat duckDataFormat = new JacksonDataFormat(Duck.class);
    JacksonDataFormat duckListDataFormat = new JacksonDataFormat(Duck[].class);

    @Override
    public void configure() throws Exception {
        from("file:src/test/resources/inputFiles?noop=true").doTry()
            .unmarshal(duckListDataFormat)
            .process(new DuckListProcessor())
            .to("direct:duckLogger")
            .to("direct:duckRestClient")
        .doCatch(Exception.class)
            .process(new ExceptionProcessor())
        .end()
        .log("DONE");
        
        from("direct:duckLogger")
            .log("Duck logger route")
            .split(simple("${body.ducks}"))
            .log("${body.name}");
            
        from("direct:duckRestClient")
            .log("Duck REST clinet route.")
            .split(simple("${body.ducks}"))
            .marshal(duckDataFormat)
            .setHeader(Exchange.HTTP_METHOD, simple("POST"))
            .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
            .to("http://localhost:8080/api/v1/duck")
            .doTry()
                .unmarshal(duckDataFormat)
                .log("POST RESPONSE ${body}")
            .doCatch(Exception.class)
                .process(new ExceptionProcessor());
            
        
    }
}